package tk.huunghia.movies.interfaces;

public interface DangKyView {
    void showWarningName(boolean wrong);
    void showWarningEmail(boolean wrong);
    void showWarningPass(boolean wrong);
    void showWarningRepass(int wrong);
    void showRegisterFail(String message);
    void showViewVideo();
    void showLoading(boolean isLoading);
}
