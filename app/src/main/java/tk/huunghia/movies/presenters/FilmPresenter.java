package tk.huunghia.movies.presenters;

import android.content.Context;
import android.util.Log;
import android.widget.CheckBox;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.database.LocalDataHelper;
import tk.huunghia.movies.interfaces.ChiTietFilmPresenter;
import tk.huunghia.movies.interfaces.ChiTietFilmView;

public class FilmPresenter implements ChiTietFilmPresenter {
    private LocalDataHelper database;
    private ChiTietFilmView view;
    public FilmPresenter(Context context, ChiTietFilmView view){
        this.database=new LocalDataHelper(context);
        this.view=view;
    }
    @Override
    public void hanleLoveClick(CheckBox ck, Boolean isLove) {
        if (isLove) {
            //save to data
            if (database.isExist(Tools.films.get(Tools.idFilm))) {
                Log.d("database", "early Love");
            } else {
                database.addRecord(Tools.films.get(Tools.idFilm));
            }
        } else {

            if (database.isExist(Tools.films.get(Tools.idFilm))) {
                database.removeRecord(Tools.films.get(Tools.idFilm));
            } else {
                Log.d("database", "removed" + Tools.films.get(Tools.idFilm).getTitle());
            }
        }
        view.updateLove( ck, isLove);
    }
    @Override
    public void handleOnStart(CheckBox checkBox) {

        if (database.isExist(Tools.films.get(Tools.idFilm))) {
            view.updateLove(checkBox, true);
        }else {
            view.updateLove(checkBox,  false);
        }
    }
}
