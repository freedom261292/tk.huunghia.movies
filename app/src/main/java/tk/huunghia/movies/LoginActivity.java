package tk.huunghia.movies;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.interfaces.DangNhapView;
import tk.huunghia.movies.presenters.LoginPresenter;

public class LoginActivity extends Activity implements DangNhapView {

    private EditText inputEmail, inputPass;
    private Button btnLogin, btnForgot;
    private LoginButton btnFacebook;
    private TextView btnRegister;
    private Activity activity = this;
    private LoginPresenter presenter;
    private ProgressBar progressBarLogin;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_login);
        init();
        inputEmail.setOnFocusChangeListener(new Tools.ListenerEdittext(this));
        inputPass.setOnFocusChangeListener(new Tools.ListenerEdittext(this));
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("AAA", "Login button");
                presenter.validateData(inputEmail.getText().toString(), inputPass.getText().toString());
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegisterUI();
            }
        });
        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgetPassUI();
            }
        });

        //access facebook
        btnFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_gender"));
        btnFacebook.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.d("fb", object.toString());
                                        presenter.handleFacebookLogin(object,loginResult.getAccessToken());
                                    }
                                });
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(activity, "Facebook login cancel", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(activity, "Facebook login - error: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    void init() {
        progressBarLogin = (ProgressBar) findViewById(R.id.progressBarLogin);
        inputEmail = (EditText) findViewById(R.id.loginInputEmail);
        inputPass = (EditText) findViewById(R.id.loginInputPass);
        btnFacebook = (LoginButton) findViewById(R.id.loginButtonFacebook);
        btnLogin = (Button) findViewById(R.id.loginButton);
        btnForgot = (Button) findViewById(R.id.loginButtonForget);
        btnRegister = (TextView) findViewById(R.id.loginButtonCallRegister);
        presenter = new LoginPresenter(activity, this);

    }

    @Override
    public void showViewVideo() {
        Log.d("AAA", "Đăng nhập thành công");
        Intent viewvideo = new Intent(LoginActivity.this, ViewActivity.class);
        startActivity(viewvideo);
    }

    @Override
    public void showLoginFail(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Message");
        builder.setMessage(message);
        builder.create().show();
    }

    @Override
    public void showRegisterUI() {
        Log.d("AAA", "Vào đăng ký");
        Intent register = new Intent(LoginActivity.this, RegistryActivity.class);
        startActivity(register);
    }

    @Override
    public void showForgetPassUI() {
        Log.d("AAA", "Vào forgot pass");
        Intent forgot = new Intent(LoginActivity.this, ForgotPassActivity.class);
        startActivity(forgot);
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) progressBarLogin.setVisibility(View.VISIBLE);
        else progressBarLogin.setVisibility(View.GONE);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (presenter.getLogin()) finish();
//        presenter.
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
