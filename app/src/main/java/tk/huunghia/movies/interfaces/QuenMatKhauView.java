package tk.huunghia.movies.interfaces;

public interface QuenMatKhauView {
    void showMessage(String message);
    void showWrong(String message);
    void showLoading(boolean isLoading);
}
