package tk.huunghia.movies.interfaces;

import android.widget.CheckBox;

import tk.huunghia.movies.data.FilmData;

public interface ChiTietFilmView {

    void updateLove(CheckBox view ,Boolean isChecked);
    void updateChiTiet();
    void readMore();
    void playFilm(String videoId);
}
