package tk.huunghia.movies.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import tk.huunghia.movies.LoginActivity;
import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.LoginFBResponse;
import tk.huunghia.movies.data.LoginFacebookData;
import tk.huunghia.movies.data.UserData;
import tk.huunghia.movies.data.remote.ApiClient;
import tk.huunghia.movies.data.ApiInterface;
import tk.huunghia.movies.data.LoginResponse;
import tk.huunghia.movies.interfaces.DangNhapPresenter;
import tk.huunghia.movies.interfaces.DangNhapView;

public class LoginPresenter implements DangNhapPresenter {
    private DangNhapView view;
    private SharedPreferences sharedPreferences;
    private Activity activity;
    private Retrofit retrofit;
    private ApiInterface api;

    public LoginPresenter(Activity activity, DangNhapView view) {
        this.activity = activity;
        this.view = view;
        sharedPreferences = this.activity.getSharedPreferences("data", Context.MODE_PRIVATE);
        retrofit = (new ApiClient()).getClient();
        api = retrofit.create(ApiInterface.class);
    }

    @Override
    public void setLogin() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("login", true);
        editor.commit();
    }

    @Override
    public boolean getLogin() {
        return sharedPreferences.getBoolean("login", false);

    }

    @Override
    public void validateData(String email, String password) {
        String message = "";
        email = email.trim();
        password = password.trim();
        if (email.length() == 0 || password.length() == 0) {
            message += "Tên/mật khẩu không được để trống\n";
            view.showLoginFail(message);
        } else {
            Log.d("Email", email + ":" + Tools.checkEmail(email) + "");
            if ((Tools.checkEmail(email))) {
                view.showLoading(true);

                api.postLogin(email, password)
                        .enqueue(new Callback<LoginResponse>() {
                            @Override
                            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                                view.showLoading(false);
                                LoginResponse data = response.body();
                                Log.d("Login", data.getError().toString() + "-" + data.getMessage());
                                if (!data.getError().booleanValue()) {
                                    setLogin();
                                    view.showViewVideo();
                                } else {
                                    view.showLoginFail(data.getMessage());
                                }

                            }

                            @Override
                            public void onFailure(Call<LoginResponse> call, Throwable t) {
                                view.showLoginFail(t.getMessage());
                                view.showLoading(false);
                            }
                        });
            } else {
                message += "Email sai dinh dang \n";
                view.showLoginFail(message);
            }
        }
    }

    @Override
    public void handleFacebookLogin(JSONObject object, AccessToken accessToken) {
        String email = "no_email@gmail.com", full_name = "", facebook_id = "", gender = "", avatar = "", facebook_token = "";
        facebook_id = accessToken.getUserId();
        facebook_token = accessToken.getToken();
        avatar = "https://graph.facebook.com/" + facebook_id + "/picture?type=normal";
        try {
            full_name = object.getString("name");
            if (object.has("email")) email = object.getString("email");
            if (object.has("user_gender")) gender = object.getString("user_gender");
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            view.showLoading(true);
            Log.d("fb", Tools.getToken() + "\n" + email + "\n" + full_name + "\n" + facebook_id + "" +
                    "\n" + gender + "\n" + avatar + "\n" + facebook_token);

            api.postLoginWithFacebook(email, full_name, facebook_id, gender, avatar, facebook_token)
                    .enqueue(new Callback<LoginFBResponse>() {
                        @Override
                        public void onResponse(Call<LoginFBResponse> call, Response<LoginFBResponse> response) {
                            view.showLoading(false);
                            LoginFBResponse data = response.body();
                            Log.d("Login", data.getError().toString() + "-" + data.getMessage());
                            if (!data.getError().booleanValue()) {
                                setLogin();
                                view.showViewVideo();
                            } else {
                                view.showLoginFail(data.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginFBResponse> call, Throwable t) {
                            view.showLoginFail("Loi: "+t.getMessage());
                            Log.d("Loi",t.getMessage());
                            view.showLoading(false);
                        }
                    });

        }


    }
}
