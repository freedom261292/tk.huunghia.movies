package tk.huunghia.movies.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilmResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("paging")
    @Expose
    private PagesFilmResponse paging;
    @SerializedName("data")
    @Expose
    private List<FilmData> data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PagesFilmResponse getPaging() {
        return paging;
    }

    public void setPaging(PagesFilmResponse paging) {
        this.paging = paging;
    }

    public List<FilmData> getData() {
        return data;
    }

    public void setData(List<FilmData> data) {
        this.data = data;
    }
}
