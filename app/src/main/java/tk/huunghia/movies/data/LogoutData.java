package tk.huunghia.movies.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogoutData {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

}
