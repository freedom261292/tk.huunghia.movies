package tk.huunghia.movies.presenters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.remote.ApiClient;
import tk.huunghia.movies.data.ApiInterface;
import tk.huunghia.movies.data.LoginResponse;
import tk.huunghia.movies.interfaces.DangKyPresenter;
import tk.huunghia.movies.interfaces.DangKyView;

public class RegisterPresenter implements DangKyPresenter {
    private DangKyView view;
    private SharedPreferences sharedPreferences;
    private Context context;

    public RegisterPresenter(Context context, DangKyView view) {
        this.view = view;
        this.context = context;
        sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);

    }

    @Override
    public void validateData(final String hoten, final String email, final String matkhau, String xacnhan) {
        boolean error = false;
        Log.d("AAA", hoten + "-" + email);
        if (hoten.trim().length() > 0) {
            view.showWarningName(false);
        } else {
            error = true;
            view.showWarningName(true);
        }
        if (email.trim().length() > 0 && Tools.checkEmail(email)) {
            view.showWarningEmail(false);
        } else {
            error = true;
            view.showWarningEmail(true);
        }
        if (matkhau.trim().length() > 0) {
            view.showWarningPass(false);
        } else {
            error = true;
            view.showWarningPass(true);
        }
        Log.d("matkhau", matkhau + "-" + xacnhan);
        if (xacnhan.trim().length() > 0 && matkhau.equals(xacnhan)) {
            view.showWarningRepass(0);//khong loi
        } else {
            error = true;
            if (xacnhan.trim().length() == 0)
                view.showWarningRepass(1);//chua nhap mat khau
            else
                view.showWarningRepass(2);//nhap xac nhan sai
        }
        if (!error) {
            view.showLoading(true);
            Retrofit retrofit = new ApiClient().getClient();
            ApiInterface api = retrofit.create(ApiInterface.class);
            api.postRegistry(email, hoten, matkhau, null, null)
                    .enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                            view.showLoading(false);
                            Log.d("AAA", hoten + "\n" +
                                    email + "\n" +
                                    matkhau);
                            LoginResponse data = response.body();
                            Log.d("AAA", data.getError().toString() + "\n"
                                    + data.getMessage() + "\n"
                                    + data.getCode());
                            if (data != null && !data.getError()) {
                                setLogin();
                                view.showViewVideo();
                            } else {
                                view.showRegisterFail(data.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            view.showRegisterFail(t.getMessage());
                            view.showLoading(false);
                        }
                    });
        }
    }

    @Override
    public void setLogin() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("login", true);
        editor.commit();
    }
}
