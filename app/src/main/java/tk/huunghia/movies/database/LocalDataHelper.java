package tk.huunghia.movies.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.FilmData;

public class LocalDataHelper extends SQLiteOpenHelper {
    private Context context;
    private static final String DATABASE_NAME = "movie";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "loved";

    private static final String KEY_ID = "id";
    private static final String KEY_MOVEID = "love_id";
    private static final String KEY_NAME = "love_name";

    public LocalDataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_MOVEID + " VARCHAR(11)," +
                KEY_NAME + " VARCHAR(255)" +
                ")";
        Log.d("DATABASE", "database: create data table" + create);
        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        Log.d("DATABASE", "database: Upgrade");
    }

    public void addRecord(FilmData note) {
        Log.d("DATABASE", "Add record " + note.getId() + " - " + note.getTitle());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MOVEID, note.getId());
        values.put(KEY_NAME, note.getTitle());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    //tim theo id film
    public boolean isExist(FilmData filmData) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[]{KEY_MOVEID},
                KEY_MOVEID + "=?",
                new String[]{filmData.getId()},
                null, null, null);
        if (cursor.getCount() > 0) {
            db.close();
            //cursor.moveToFirst();
            //cursor.getString(0),1,2....
            return true;
        }
        return false;
    }

    public boolean removeRecord(FilmData film) {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("database", "removed " + film.getTitle());
        int del = db.delete(TABLE_NAME, KEY_MOVEID + "=?", new String[]{film.getId()});
        if (del > 0) return true;
        else return false;

    }
}
