package tk.huunghia.movies.presenters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.CheckBox;

import com.facebook.AccessToken;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.huunghia.movies.data.ApiInterface;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.data.FilmResponse;
import tk.huunghia.movies.database.LocalDataHelper;
import tk.huunghia.movies.interfaces.MainListPresenter;
import tk.huunghia.movies.interfaces.MainListView;

public class MainListPresenterImplement implements MainListPresenter {
    private MainListView view;
    private Context context;
    private LocalDataHelper database;
    private SharedPreferences sharedPreferences;

    public MainListPresenterImplement(MainListView view, Context context) {
        this.view = view;
        this.context = context;
        database = new LocalDataHelper(context);
        sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
    }

    @Override
    public void getListFilm(ApiInterface api,
                            final int page,
                            int perPage) {

        if (!view.getIsLoading()) return;
        api.getFilm( page, perPage)
                .enqueue(new Callback<FilmResponse>() {
                    @Override
                    public void onResponse(Call<FilmResponse> call, Response<FilmResponse> response) {
                        if (response.body() != null) {
                            Log.d("AAA", "page: " + page);
                            Log.d("AAA", "total item" + response.body().getPaging().getTotalItem());
                            List<FilmData> movies = response.body().getData();
                            view.refreshListview(movies);
                            view.setLoading(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<FilmResponse> call, Throwable t) {
                        view.setLoading(false);
                        view.showErrorConnection(t.getMessage());
                        Log.d("AAA", "Timeout: " + t.getMessage());
                    }
                });
    }

    @Override
    public void setLogin(boolean isLogin) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("login", isLogin);
        editor.commit();
    }

    @Override
    public boolean getLogin() {

//check fb is login
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        return sharedPreferences.getBoolean("login", false) ;//| isLoggedIn;
    }

    @Override
    public void hanleLoveClick(CheckBox checkBox, FilmData film, Boolean isLove) {
        if (isLove) {
            //save to data
            if (database.isExist(film)) {
                Log.d("database", "early Love");
            } else {
                database.addRecord(film);
            }
        } else {

            if (database.isExist(film)) {
                database.removeRecord(film);
            } else {
                Log.d("database", "removed" + film.getTitle());
            }
        }
        view.updateLove(checkBox, film, isLove);
    }

    @Override
    public void handleOnStart(CheckBox checkBox, FilmData filmData) {
        if (database.isExist(filmData)) {
            view.updateLove(checkBox, filmData, true);
        } else {
            view.updateLove(checkBox, filmData, false);
        }
    }

    @Override
    public void handleViewClick() {
        if (getLogin()) {
            view.doViewUI();
        } else {
            view.doLoginUI();
        }

    }
}
