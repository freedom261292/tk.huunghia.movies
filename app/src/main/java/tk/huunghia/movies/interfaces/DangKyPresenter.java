package tk.huunghia.movies.interfaces;

public interface DangKyPresenter {
    void validateData(String hoten, String email, String matkhau, String xacnhan);
    void setLogin();
}
