package tk.huunghia.movies;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.interfaces.DangKyView;
import tk.huunghia.movies.presenters.RegisterPresenter;

public class RegistryActivity extends Activity implements DangKyView {
    private TextView alertHoTen, alertEmail, alertPass, alertRepass, dieuKhoan, quyDinhBaomat;
    private EditText name, email, pass, repass;
    private Button register;
    private ImageButton cancel;
    private Activity activity = this;
    private RegisterPresenter presenter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);
        init();
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateData(name.getText().toString(),
                        email.getText().toString(),
                        pass.getText().toString(),
                        repass.getText().toString());
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.progressBarRegister);
        presenter = new RegisterPresenter(activity, this);
        alertEmail = (TextView) findViewById(R.id.registerErrorEmail);
        alertHoTen = (TextView) findViewById(R.id.registerErrorName);
        alertPass = (TextView) findViewById(R.id.registerErrorPass);
        alertRepass = (TextView) findViewById(R.id.registerErrorPassConfirm);
        name = (EditText) findViewById(R.id.registerInputName);
        email = (EditText) findViewById(R.id.registerInputEmail);
        pass = (EditText) findViewById(R.id.registerInputPass);
        repass = (EditText) findViewById(R.id.registerInputPassConfirm);
        register = (Button) findViewById(R.id.registerBtn);
        cancel = (ImageButton) findViewById(R.id.registerBtnCancel);
        Tools.ListenerEdittext listener = new Tools.ListenerEdittext(activity);
        name.setOnFocusChangeListener(listener);
        email.setOnFocusChangeListener(listener);
        pass.setOnFocusChangeListener(listener);
        repass.setOnFocusChangeListener(listener);
    }

    @Override
    public void showWarningName(boolean wrong) {
        if (wrong) {
            alertHoTen.setText("Bạn chưa điền Họ Tên");
        } else {
            alertHoTen.setText(" ");

        }
    }

    @Override
    public void showWarningEmail(boolean wrong) {
        if (wrong) {
            alertEmail.setText("Bạn chưa điền Email");
        } else {
            alertEmail.setText(" ");

        }
    }

    @Override
    public void showWarningPass(boolean wrong) {
        if (wrong) {
            alertPass.setText("Bạn chưa điền mật khẩu");
        } else {
            alertPass.setText(" ");

        }
    }

    @Override
    public void showWarningRepass(int wrong) {
        switch(wrong) {
            case 1:
                alertRepass.setText("Bạn chưa xác nhận mật khẩu");
                break;
            case 2:
                alertRepass.setText("Xác nhận mật khẩu sai");
                break;
            default:
                alertRepass.setText(" ");
        }

    }

    @Override
    public void showRegisterFail(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Message");
        builder.setMessage(message);
        builder.create().show();
    }

    @Override
    public void showViewVideo() {
        Log.d("AAA", "Đăng nhập thành công");
        Intent viewvideo = new Intent(RegistryActivity.this, ViewActivity.class);
        startActivity(viewvideo);
        finish();
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            cancel.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            cancel.setVisibility(View.VISIBLE);
        }
    }
}
