package tk.huunghia.movies.interfaces;

import android.widget.CheckBox;

import tk.huunghia.movies.data.FilmData;

public interface ChiTietFilmPresenter {
    void hanleLoveClick(CheckBox view, Boolean isLove);
    void handleOnStart(CheckBox checkBox);
}
