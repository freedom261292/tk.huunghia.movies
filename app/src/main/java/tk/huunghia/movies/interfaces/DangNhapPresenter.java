package tk.huunghia.movies.interfaces;

import com.facebook.AccessToken;

import org.json.JSONObject;

public interface DangNhapPresenter {
    void setLogin();
    boolean getLogin();
    void validateData(String email, String password);
    void handleFacebookLogin(JSONObject object, AccessToken accessToken);
}
