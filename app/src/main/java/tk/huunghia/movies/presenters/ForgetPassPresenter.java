package tk.huunghia.movies.presenters;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.remote.ApiClient;
import tk.huunghia.movies.data.ApiInterface;
import tk.huunghia.movies.interfaces.QuenMatKhauPresenter;
import tk.huunghia.movies.interfaces.QuenMatKhauView;

public class ForgetPassPresenter implements QuenMatKhauPresenter {
    private QuenMatKhauView view;

    public ForgetPassPresenter(QuenMatKhauView view) {
        this.view = view;
    }

    @Override
    public void validateForgot(String email) {
        view.showLoading(true);
        if (Tools.checkEmail(email)) {
            Retrofit retrofit = new ApiClient().getClient();
            ApiInterface api = retrofit.create(ApiInterface.class);
            api.postForget(email).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    view.showMessage("Send email");
                    view.showLoading(false);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    view.showWrong(t.getMessage());
                    view.showLoading(false);
                }
            });

        }else {
            view.showWrong("Sai dinh dang email");
            view.showLoading(false);
        }
    }
}
