package tk.huunghia.movies.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PagesFilmResponse {

    @SerializedName("total_item")
    @Expose
    private Integer totalItem;
    @SerializedName("per_page")
    @Expose
    private Integer  perPage;
    @SerializedName("current_page")
    @Expose
    private Integer  currentPage;
    @SerializedName("total_page")
    @Expose
    private Integer  totalPage;

    public Integer getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(Integer totalItem) {
        this.totalItem = totalItem;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
