package tk.huunghia.movies.interfaces;

import android.widget.CheckBox;

import tk.huunghia.movies.data.ApiInterface;
import tk.huunghia.movies.data.FilmData;

public interface MainListPresenter {
    void getListFilm(ApiInterface api,
                               int page,
                               int perPage);
    void setLogin(boolean isLogin);
    boolean getLogin();
    void hanleLoveClick(CheckBox view,FilmData filmData, Boolean isLove);

    void handleOnStart(CheckBox checkBox, FilmData filmData);

    //check view video click-> login if not login or view
    void handleViewClick();
}
