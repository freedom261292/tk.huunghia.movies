package tk.huunghia.movies;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.presenters.MainListPresenterImplement;

public class AdapterListMovie extends BaseAdapter {
    private Activity activity;
    private Integer layout;
    MainListPresenterImplement presenter;

    public AdapterListMovie(Activity activity, Integer layout, MainListPresenterImplement presenter) {
        this.activity = activity;
        this.layout = layout;
        this.presenter = presenter;
    }

    @Override
    public int getCount() {
        return Tools.films.size();
    }

    @Override
    public Object getItem(int position) {
        return Tools.films.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView itemFilmImg;
        TextView itemFilmNameEn;
        TextView itemFilmNameVI;
        TextView itemFilmViewCount;
        TextView itemFilmDescription;
        CheckBox itemFilmLoved;
        Button itemFilmBtnView;

        ViewHolder(View view) {
            itemFilmImg = (ImageView) view.findViewById(R.id.itemFilmImg);
            itemFilmNameEn = (TextView) view.findViewById(R.id.txtFilmNameEN);
            itemFilmNameVI = (TextView) view.findViewById(R.id.txtFilmNameVI);
            itemFilmViewCount = (TextView) view.findViewById(R.id.txtFilmViewCount);
            itemFilmDescription = (TextView) view.findViewById(R.id.txtFilmDescription);
            itemFilmLoved = (CheckBox) view.findViewById(R.id.itemFilmCheckLove);
            itemFilmBtnView = (Button) view.findViewById(R.id.itemFilmBtnView);
        }

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        final FilmData film = Tools.films.get(position);
        Log.d("Item", position + "");
        List<String> names = Arrays.asList(film.getTitle().split(" / "));

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(activity.getApplicationContext());
            convertView = inflater.inflate(R.layout.list_item_film, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Picasso.get()
                .load(film.getImage())
                .placeholder(R.drawable.loading)
                .error(R.drawable.no_image_available)
                .into(viewHolder.itemFilmImg);
        viewHolder.itemFilmNameEn.setText(names.get(0));
        viewHolder.itemFilmNameVI.setText(names.get(names.size() - 1));
        viewHolder.itemFilmViewCount.setText("Lượt xem: 99999");
        viewHolder.itemFilmDescription.setText(film.getDescription());
        //set 3 dot description
        final ViewTreeObserver vto = viewHolder.itemFilmDescription.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ViewTreeObserver vto2 = viewHolder.itemFilmDescription.getViewTreeObserver();
                vto2.removeOnGlobalLayoutListener(this);
                int lineEndIndex = viewHolder.itemFilmDescription.getLayout().getLineEnd(2);
                if(lineEndIndex>3){
                    String txt = viewHolder.itemFilmDescription.getText().subSequence(0, lineEndIndex - 3) + "...";
                    viewHolder.itemFilmDescription.setText(txt);
                    Tools.justify(viewHolder.itemFilmDescription);
                }


            }
        });

        presenter.handleOnStart(viewHolder.itemFilmLoved, film);

        viewHolder.itemFilmLoved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                presenter.hanleLoveClick(viewHolder.itemFilmLoved, film, isChecked);
            }
        });
        viewHolder.itemFilmBtnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.idFilm=position;
                presenter.handleViewClick();
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("AAA", "Item click: " + position);
            }
        });
        return convertView;
    }
}
