package tk.huunghia.movies.interfaces;

import android.widget.CheckBox;

import java.util.List;

import tk.huunghia.movies.data.FilmData;

public interface MainListView {
    void refreshListview(List<FilmData> data);
    void setLoading(Boolean isLoading);
    boolean getIsLoading();
    void updateLove(CheckBox view ,FilmData filmData, Boolean isChecked);
    void doLoginUI();
    void doViewUI();

    void showErrorConnection(String message);
}
