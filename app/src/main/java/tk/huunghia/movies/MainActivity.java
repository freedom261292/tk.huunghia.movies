package tk.huunghia.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;

import java.util.List;

import retrofit2.Retrofit;
import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.ApiInterface;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.data.remote.ApiClient;
import tk.huunghia.movies.interfaces.MainListView;
import tk.huunghia.movies.presenters.MainListPresenterImplement;

public class MainActivity extends Activity implements MainListView {


    private LinearLayout toolbar;
    private ListView listViewMovie;
    private ProgressBar loadingBar;
    private Activity activity = this;
    private TextView titleBar;
    private AdapterListMovie adapterListMovie;
    Retrofit retrofit;
    ApiInterface api;
    int page = 1, perPage = 10;
    int SCROLL_STATE = 0;
    boolean isLoading = true;
    MainListPresenterImplement presenter;
    Handler handler = new Handler();
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_main);
        init();
        presenter.getListFilm(api,  page, perPage);
        listViewMovie.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                SCROLL_STATE = scrollState;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //vị trí cuối cùng trong list view
                Log.d("AAA", "Scroll state" + SCROLL_STATE);
                if (view.getLastVisiblePosition() == totalItemCount - 1 &&
                        isLoading == false &&
                        SCROLL_STATE == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    isLoading = true;
                    setLoading(isLoading);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            presenter.getListFilm(api,  ++page, perPage);
                        }
                    }, 3000);

                }
                Log.d("AAA", firstVisibleItem + "-" + visibleItemCount + "-" + totalItemCount);
            }
        });
    }

    void init() {
        toolbar = (LinearLayout) findViewById(R.id.toolBar);
        listViewMovie = (ListView) findViewById(R.id.listView);
        loadingBar = (ProgressBar) findViewById(R.id.progressBar);
        titleBar = (TextView) findViewById(R.id.titleBar);
        listViewMovie.setAdapter(adapterListMovie);

        retrofit = (new ApiClient()).getClient();
        api = retrofit.create(ApiInterface.class);
        presenter = new MainListPresenterImplement(this, activity);
        adapterListMovie = new AdapterListMovie(this.activity, R.layout.list_item_film, presenter);
        listViewMovie.setAdapter(adapterListMovie);
    }

    @Override
    public void refreshListview(List<FilmData> data) {
        if (data.size() == 0) {
            page--;
            Log.d("AAA", "het roi");
            Log.d("AAA", "page:" + page);
            Toast.makeText(activity, "Hết phim mới rồi bạn ơi!", Toast.LENGTH_SHORT).show();

            return;
        }
        if (Tools.films.containsAll(data)) return;
        Tools.films.addAll(data);
        adapterListMovie.notifyDataSetChanged();
        Log.d("film", Tools.films.size() + ": " + data.size());
    }

    @Override
    public void setLoading(Boolean isLoading) {
        this.isLoading = isLoading;
        if (isLoading) loadingBar.setVisibility(View.VISIBLE);
        else loadingBar.setVisibility(View.GONE);
    }

    @Override
    public boolean getIsLoading() {
        return isLoading;
    }

    @Override
    public void updateLove(CheckBox view, FilmData film, Boolean isChecked) {
        view.setChecked(isChecked);
        Log.d("AAA", "updateLove");
        if (isChecked) {
            view.setButtonDrawable(R.drawable.ic_like_orange);
            view.setText(activity.getResources().getText(R.string.dathich));
            view.setTextColor(activity.getResources().getColor(R.color.toolbar_bgcolor));
        } else {
            view.setButtonDrawable(R.drawable.ic_like);
            view.setText(activity.getResources().getText(R.string.thich));
            view.setTextColor(activity.getResources().getColor(R.color.white));
        }
    }

    @Override
    public void doLoginUI() {
        Intent login = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(login);
    }

    @Override
    public void doViewUI() {
        Log.d("AAA", "doViewUI");
        Intent viewvideo = new Intent(MainActivity.this, ViewActivity.class);
        startActivity(viewvideo);
    }

    @Override
    public void showErrorConnection(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapterListMovie.notifyDataSetChanged();

    }

    @Override
    protected void onDestroy() {
        //logout
        presenter.setLogin(false);
        //logout facebook
        LoginManager.getInstance().logOut();
        super.onDestroy();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
