package tk.huunghia.movies.interfaces;

import tk.huunghia.movies.data.FilmData;

public interface DangNhapView {
    void showViewVideo();
    void showLoginFail(String message);
    void showRegisterUI();
    void showForgetPassUI();
    void showLoading(boolean isLoading);

}
