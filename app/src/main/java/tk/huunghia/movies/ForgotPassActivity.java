package tk.huunghia.movies;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.interfaces.QuenMatKhauView;
import tk.huunghia.movies.presenters.ForgetPassPresenter;

public class ForgotPassActivity extends Activity implements QuenMatKhauView {
    EditText email;
    Button sendLink, finish;
    ImageButton close;
    ForgetPassPresenter presenter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        init();
        email.setOnFocusChangeListener(new Tools.ListenerEdittext(this));
        sendLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateForgot(email.getText().toString());
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void init() {
        progressBar=(ProgressBar) findViewById(R.id.progressBarForgotPass);
        email = (EditText) findViewById(R.id.forgetInputEmail);
        sendLink = (Button) findViewById(R.id.forgetPassButtonSend);
        close = (ImageButton) findViewById(R.id.forgetPassButtonCancel);
        finish = (Button) findViewById(R.id.forgetPassButtonFinish);
        presenter = new ForgetPassPresenter(this);
    }

    @Override
    public void showMessage(String message) {
        finish.setVisibility(View.VISIBLE);
        close.setVisibility(View.GONE);
        Toast.makeText(ForgotPassActivity.this, message, Toast.LENGTH_SHORT).show();
        Log.d("Fogot", message);
    }

    @Override
    public void showWrong(String message) {
        Log.d("Fogot", message);
        Toast.makeText(ForgotPassActivity.this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoading(boolean isLoading) {
        if(isLoading) progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.GONE);
    }
}
