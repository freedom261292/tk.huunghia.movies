package tk.huunghia.movies.data;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("movie/list")
    Call<FilmResponse> getFilm(
            @Query("page") Integer  page,
            @Query("per_page") Integer perPage
    );
    @FormUrlEncoded
    @POST("user/login")
    Call<LoginResponse> postLogin(
            @Field("email") String email ,
            @Field("password") String password
    );
    @FormUrlEncoded
    @POST("user/social-login")
    Call<LoginFBResponse> postLoginWithFacebook(
            @Field("email") String email,
            @Field("full_name") String full_name,
            @Field("facebook_id") String facebook_id,
            @Field("gender") String gender,
            @Field("avatar") String avatar,
            @Field("facebook_token") String facebook_token
    );

    @FormUrlEncoded
    @POST("user/registry")
    Call<LoginResponse> postRegistry(
            @Field("email") String email,
            @Field("full_name")String full_name,
            @Field("password")String password,
            @Field("gender")String gender,
            @Field("birthday")String birthday
    );

    @POST("user/logout")
    Call<LogoutData> postLogout();

    @POST("user/forgot-password")
    Call<Void> postForget(@Query("email")String email
    );

}
