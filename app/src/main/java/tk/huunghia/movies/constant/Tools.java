package tk.huunghia.movies.constant;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import tk.huunghia.movies.R;
import tk.huunghia.movies.data.FilmData;

public class Tools {
    public static ArrayList<FilmData> films = new ArrayList<>();
    public static int idFilm = -1;

    public static void printKeyHash(Context context) {
        //get keyhash app for facebook

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public static String getToken() {
        return "dCuW7UQMbdvpcBDfzolAOSGFIcAec11a";
    }

    public static void justify(final TextView textView) {

        final AtomicBoolean isJustify = new AtomicBoolean(false);

        final String textString = textView.getText().toString();

        final TextPaint textPaint = textView.getPaint();

        final SpannableStringBuilder builder = new SpannableStringBuilder();

        textView.post(new Runnable() {
            @Override
            public void run() {

                if (!isJustify.get()) {

                    final int lineCount = textView.getLineCount();
                    final int textViewWidth = textView.getWidth();

                    for (int i = 0; i < lineCount; i++) {

                        int lineStart = textView.getLayout().getLineStart(i);
                        int lineEnd = textView.getLayout().getLineEnd(i);
                        try {
                            String lineString = textString.substring(lineStart, lineEnd);

                            if (i == lineCount - 1) {
                                builder.append(new SpannableString(lineString));
                                break;
                            }

                            String trimSpaceText = lineString.trim();
                            String removeSpaceText = lineString.replaceAll(" ", "");

                            float removeSpaceWidth = textPaint.measureText(removeSpaceText);
                            float spaceCount = trimSpaceText.length() - removeSpaceText.length();

                            float eachSpaceWidth = (textViewWidth - removeSpaceWidth) / spaceCount;

                            SpannableString spannableString = new SpannableString(lineString);
                            for (int j = 0; j < trimSpaceText.length(); j++) {
                                char c = trimSpaceText.charAt(j);
                                if (c == ' ') {
                                    Drawable drawable = new ColorDrawable(0x00ffffff);
                                    drawable.setBounds(0, 0, (int) eachSpaceWidth, 0);
                                    ImageSpan span = new ImageSpan(drawable);
                                    spannableString.setSpan(span, j, j + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                }
                            }

                            builder.append(spannableString);
                        } catch (Exception e) {
                            Log.d("AppError", e.getMessage());
                        }

                    }

                    textView.setText(builder);
                    isJustify.set(true);
                }
            }
        });
    }

    public static boolean checkEmail(String email) {
        return (email.trim().length() > 0) &&
                Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static class ListenerEdittext implements View.OnFocusChangeListener {
        private Context context;

        public ListenerEdittext(Context context) {
            this.context = context;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            Log.d("AAA", "set text alpha");
            if (!hasFocus) {
                ((EditText) v).setTextColor(Color.WHITE);
            } else {
                ((EditText) v).setTextColor(ContextCompat.getColor(context,
                        R.color.whiteAlpha70));
            }
        }
    }
}
