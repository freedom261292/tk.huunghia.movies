package tk.huunghia.movies;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import tk.huunghia.movies.constant.Tools;
import tk.huunghia.movies.data.FilmData;
import tk.huunghia.movies.interfaces.ChiTietFilmView;
import tk.huunghia.movies.presenters.FilmPresenter;

public class ViewActivity extends Activity implements ChiTietFilmView {
    private ImageView filmImg;
    private TextView filmNameEn, filmNameVI, filmViewCount, filmDuration, filmActor;
    private TextView filmDescription, filmGenres, filmDirector, filmManufacturer;
    private CheckBox filmLoved;
    private Button readMore;
    Activity activity = this;
    FilmPresenter filmPresenter;
    WebView videoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        init();
        updateChiTiet();
        readMore();
        filmNameEn.setSelected(true);
        filmPresenter.handleOnStart(filmLoved);
        filmLoved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                filmPresenter.hanleLoveClick(filmLoved, isChecked);
            }
        });
    }

    void init() {
        readMore = (Button) findViewById(R.id.viewFilmReadMore);
        filmImg = (ImageView) findViewById(R.id.viewFilmImg);
        filmNameEn = (TextView) findViewById(R.id.viewFilmNameEn);
        filmNameVI = (TextView) findViewById(R.id.viewFilmNameVI);
        filmViewCount = (TextView) findViewById(R.id.viewFilmViewCount);
        filmDescription = (TextView) findViewById(R.id.viewFilmDescription);
        filmGenres = (TextView) findViewById(R.id.viewFilmGenres);
        filmActor = (TextView) findViewById(R.id.viewFilmActor);
        filmDirector = (TextView) findViewById(R.id.viewFilmDirector);
        filmManufacturer = (TextView) findViewById(R.id.viewFilmManufacturer);
        filmDuration = (TextView) findViewById(R.id.viewFilmDuration);
        filmLoved = (CheckBox) findViewById(R.id.viewFilmCheckLove);
        filmPresenter = new FilmPresenter(activity, this);
        videoPlayer = findViewById(R.id.viewFilmPlayer);

    }

    @Override
    public void updateLove(CheckBox view, Boolean isChecked) {
        Log.d("AAA", "updateLove");
        if (isChecked) {
            view.setChecked(isChecked);
            view.setButtonDrawable(R.drawable.ic_like_orange);
            view.setText(activity.getResources().getText(R.string.dathich));
            view.setTextColor(activity.getResources().getColor(R.color.toolbar_bgcolor));
        } else {
            view.setChecked(isChecked);
            view.setButtonDrawable(R.drawable.ic_like);
            view.setText(activity.getResources().getText(R.string.thich));
            view.setTextColor(activity.getResources().getColor(R.color.white));

        }
    }

    @Override
    public void updateChiTiet() {
        Log.d("AAA", "Chi tiet film: " + Tools.films.get(Tools.idFilm).getTitle());
        List<String> names = Arrays.asList(Tools.films.get(Tools.idFilm).getTitle().split(" / "));
        Picasso.get()
                .load(Tools.films.get(Tools.idFilm).getImage())
                .placeholder(R.drawable.loading)
                .error(R.drawable.no_image_available)
                .into(filmImg);
        filmNameEn.setText(names.get(0));
        filmNameVI.setText(names.get(names.size() - 1));
        filmViewCount.setText("Lượt xem: 99999");
        formatText();
        playFilm(Tools.films.get(Tools.idFilm).getLink());
        filmLoved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                filmPresenter.hanleLoveClick(filmLoved, isChecked);
            }
        });
        readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readMore();

            }
        });
    }

    private void formatText() {
        String textTitle = activity.getResources().getText(R.string.genres).toString();
        String textContent = Tools.films.get(Tools.idFilm).getCategory();
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(textTitle + textContent);
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, textTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        filmGenres.setText(stringBuilder);

        stringBuilder.clear();
        textTitle = activity.getResources().getText(R.string.actor).toString();
        textContent = Tools.films.get(Tools.idFilm).getActor();
        stringBuilder = new SpannableStringBuilder(textTitle +" "+ textContent);
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, textTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        filmActor.setText(stringBuilder);

        stringBuilder.clear();
        textTitle = activity.getResources().getText(R.string.director).toString();
        textContent = Tools.films.get(Tools.idFilm).getDirector();
        stringBuilder = new SpannableStringBuilder(textTitle  +" "+  textContent);
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, textTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        filmDirector.setText(stringBuilder);

        stringBuilder.clear();
        textTitle = activity.getResources().getText(R.string.manufacturer).toString();
        textContent = Tools.films.get(Tools.idFilm).getManufacturer();
        stringBuilder = new SpannableStringBuilder(textTitle  +" "+  textContent);
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, textTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        filmManufacturer.setText(stringBuilder);

        stringBuilder.clear();
        textTitle = activity.getResources().getText(R.string.duration).toString();
        textContent = Tools.films.get(Tools.idFilm).getDuration()  +" "+
                activity.getResources().getString(R.string.minute);
        stringBuilder = new SpannableStringBuilder(textTitle  +" "+  textContent);
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, textTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        filmDuration.setText(stringBuilder);

        filmDescription.setText(Tools.films.get(Tools.idFilm).getDescription());

        ViewTreeObserver viewTreeObserver = filmDescription.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ViewTreeObserver viewTreeObserver1 = filmDescription.getViewTreeObserver();
                viewTreeObserver1.removeOnGlobalLayoutListener(this);
                if (filmDescription.getLineCount() > 3) {
                    Log.d("AAA", filmDescription.getLineCount() + "-" + filmDescription.getText());
                    int lineEndIndex = filmDescription.getLayout().getLineEnd(2);
                    String text = filmDescription.getText().subSequence(0, lineEndIndex - 3) + "...";
                    Log.d("AAA", "NewText:" + text);
                    filmDescription.setText(text);
                    Tools.justify(filmDescription);
                    readMore.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void readMore() {
        readMore.setVisibility(View.GONE);
        filmDescription.setText(Tools.films.get(Tools.idFilm).getDescription());
        Tools.justify(filmDescription);
    }

    @Override
    public void playFilm(String link) {
        String[] temp = link.split("=");
        String videoID = temp[1];
        videoPlayer.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = videoPlayer.getSettings();
        webSettings.setJavaScriptEnabled(true);
        Log.d("AAA", "video: " + videoID);
        videoPlayer.loadUrl("http://www.youtube.com/embed/" + videoID + "?autoplay=1");
    }
}
